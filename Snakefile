import os
import re
from glob import glob

include: "rules/common.smk"

def ref_prefixs():
    return [os.path.splitext(x)[0] for x in glob("inputs/ref/*.gbk")]

def list_fasta_refs():
    return [x + ".fasta" for x in ref_prefixs()]

def chose_reference(sample):
    sname = os.path.basename(sample).split("_")[0]
    # print(sname)
    with open("etc/sample2donor.dat", 'r') as file:
        for line in file:
            s, r = line.rstrip().split("\t")
            if s == sname:
                return f"inputs/ref/{r}"


def chose_ref_gbk(sample):
    fasta = chose_reference(sample)
    return re.sub(r'fasta', r'gbk', fasta)


def list_samples():
    return list(set([re.split("_[12].fq.gz", os.path.basename(x))[0]\
                     for x in glob("inputs/fq/*.fq.gz")]))

def get_stem(filename):
    return os.path.basename(os.path.splitext(filename)[0])

recip  = [get_stem(x) for x in glob("inputs/ref/*.fasta") if re.match(".*RECIPIENT\.fasta", x)]
donors = [get_stem(x) for x in glob("inputs/ref/*.gbk") if re.match(".*DONOR[12]\.gbk", x)]


def list_samples_of_donor1():
    ls = list_samples()
    sn = [x for x in ls if re.match(".*DONOR1\.gbk", chose_ref_gbk(x))]
    sn.sort()
    return sn


def list_samples_of_donor2():
    ls = list_samples()
    sn = [x for x in ls if re.match(".*DONOR2\.gbk", chose_ref_gbk(x))]
    sn.sort()
    return sn


# TARGETS ------------------------------------------------------------

rule all_merged_vcf:
    input: expand("outputs/donor{n}.vcf.gz", n=[1,2])

rule all_recip2donor:
    input: expand("outputs/{donor}-{recip}/snps.vcf.gz", recip=recip, donor=donors)

rule all_variant:
    input: expand("outputs/{sample}/snps.vcf.gz", sample = list_samples())

rule all_subsample:
    input: expand("outputs/fq/{sample}.sub_{dir}.fq.gz", sample = list_samples(), dir = [1, 2])

rule all_reference:
    input: [list_fasta_refs(), "etc/sample2donor.dat"]


# RULEs --------------------------------------------------------------
ruleorder: call_donor_variants > call_recip_variants > fq_subsample

localrules: gbk2fasta
rule gbk2fasta:
    input: "{file}.gbk"
    output: "{file}.fasta"
    shell: "seqret -sequence {input} -outseq {output}"


localrules: sample2donor_table
rule sample2donor_table:
    input: "etc/Description_files.csv"
    output: "etc/sample2donor.dat"
    shell: "Rscript scripts/create_donor2sample_match.R"


rule fq_subsample:
    input:
        R1 = "inputs/fq/{sample}_1.fq.gz",
        R2 = "inputs/fq/{sample}_2.fq.gz",
        mdat = "etc/sample2donor.dat"
    output:
        R1 = "outputs/fq/{sample}.sub_1.fq.gz",
        R2 = "outputs/fq/{sample}.sub_2.fq.gz"
    params:
        coverage = 50
    threads: 4
    run:
        ref = chose_reference(wildcards.sample)
        shell("fq-subsample --in {input} --out {output} --ref {ref} --depth {params.coverage} --cpus {threads}")

rule call_recip_variants:
    input:
        recip = "inputs/ref/{recip}.fasta",
        donor = "inputs/ref/{donor}.gbk",
    output: "outputs/{donor}-{recip,RECIP}/snps.vcf.gz"
    threads: 8
    params:
        outdir = "outputs/{donor}-{recip}"
    log: ".log/{recip}-{donor}_err.log"
    resources:
        runtime = runtimer(2 * 60),
        mem_mb  = mem_gb(16)
    shell:
        # --force is because outdir is created by snkmk
        "snippy --force --cpus {threads} --outdir {params.outdir} --ref {input.donor} --ctgs {input.recip} 2> {log}"


rule call_donor_variants:
    input:
        donor_fasta = "inputs/ref/{donor}.fasta",
        donor_gbk   = "inputs/ref/{donor}.gbk",
    output: "outputs/{donor}-{donor}/snps.vcf.gz"
    threads: 8
    params:
        outdir = "outputs/{donor}-{donor}"
    log: ".log/{donor}-{donor}_err.log"
    resources:
        runtime = runtimer(30),
        mem_mb  = mem_gb(16)
    shell:
        # --force is because outdir is created by snkmk
        "snippy --force --cpus {threads} --outdir {params.outdir} --ref {input.donor_gbk} --ctgs {input.donor_fasta} 2> {log}"


rule call_variants:
    input:
        R1 = rules.fq_subsample.output.R1,
        R2 = rules.fq_subsample.output.R2,
    output:
        vcf = "outputs/{sample}/snps.vcf.gz"
    threads: 8
    params:
        outdir = "outputs/{sample}"
    log: ".log/{sample}_snippy_err.log"
    resources:
        runtime = runtimer(2 * 60),
        mem_mb  = mem_gb(16)
    run:
        ref = chose_ref_gbk(wildcards.sample)
        # --force is because outdir is created by snkmk
        shell("snippy --force --cpus {threads} --outdir {params.outdir} --ref {ref} --R1 {input.R1} --R2 {input.R2} 2> {log}")

# MERGE VCF ----------------------------------------------------------
rule merge_vcf_donor_1:
    input:
        vcfs=[expand("outputs/{sample}/snps.vcf.gz", sample=list_samples_of_donor1()),\
              f"outputs/{donors[0]}-{recip[0]}/snps.vcf.gz", \
              f"outputs/{donors[0]}-{donors[0]}/snps.vcf.gz"]
    output:
        vcf="outputs/donor1.vcf.gz"
    resources:
        runtime = runtimer(4),
        mem_mb  = mem_gb(4)
    shell:
        "bcftools merge -Oz -o {output} {input}"

rule merge_vcf_donor_2:
    input:
        vcfs=[expand("outputs/{sample}/snps.vcf.gz", sample=list_samples_of_donor2()),\
              f"outputs/{donors[1]}-{recip[0]}/snps.vcf.gz",\
              f"outputs/{donors[1]}-{donors[1]}/snps.vcf.gz"]
    output:
        vcf="outputs/donor2.vcf.gz"
    threads: 4
    resources:
        runtime = runtimer(4),
        mem_mb  = mem_gb(4)
    shell:
        "bcftools merge --threads {threads} -Oz -o {output} {input}"
