
def runtimer(time):
    """
    Return a function that increase the runtime at each job attempt.
    """
    def sk(w, attempt):
        return int(attempt**attempt * time)
    return sk

def add_cpu(ncpu):
    """
    Increase the number of CPU at each task attempt.
    """
    def cpu(w, attempt):
        return int(attempt * ncpu)
    return cpu

def mem_gb(gb):
    """
    Return the memory usage in Mb, multiplied by the number of job
    attempt.
    """
    mb = int(gb * 1024)
    def f(w, attempt):
        return int(attempt * mb)
    return f
